## Getting started

  - Create "Freemium" account [here](https://developer.here.com/documentation/android-premium/dev_guide/topics/quick-start.html)
  - Download the SDK zip file and place HERE-sdk.aar inside app/libs/ folder
  - Change `applicationId "com.etergo.heresdk.examples"` to your app package in app/build.gradle
  - Include 3 string resources: `<string name="here_appid">YOUR_APP_ID</string>`, `<string name="here_apptoken">YOUR_APP_TOKEN</string>` and `<string name="here_license_key">YOUR_LICENSE_KEY</string>`
  
## Reproducing the problem

  - Run the sample app, grant it all permissions and click the "Locate Me" button
  - Notice the logs
  
## Observation

  - If you have some sort of mock GPS app running (like FakeGPS - make sure you also set it as mock location app in Developer Options), then you notice in logs that "network" location provider is enabled. In this case, the SDK can find your location
  - If you don't have any mock GPS app running, you see "network" location provider is not there. You see `onNetworkLocationDisabled` callback from the SDK. In this case, it cannot find your location  