package com.etergo.gpsdetect

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.provider.Settings.Secure.LOCATION_MODE_BATTERY_SAVING
import android.provider.Settings.Secure.LOCATION_MODE_HIGH_ACCURACY
import android.provider.Settings.Secure.LOCATION_MODE_OFF
import android.provider.Settings.Secure.LOCATION_MODE_SENSORS_ONLY
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.location.LocationManagerCompat
import com.here.android.mpa.common.ApplicationContext
import com.here.android.mpa.common.GeoPosition
import com.here.android.mpa.common.LocationDataSourceHERE
import com.here.android.mpa.common.MapEngine
import com.here.android.mpa.common.MapSettings
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.common.PositioningManager
import com.here.android.mpa.common.Version
import com.here.android.mpa.mapping.Map
import com.here.android.positioning.StatusListener
import com.here.android.positioning.StatusListenerAdapter
import kotlinx.android.synthetic.main.act_main.*
import java.io.File
import java.lang.ref.WeakReference

private const val USE_NETWORK_POSITIONING = true

class MainActivity: AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    private val locationManager by lazy {
        getSystemService(LocationManager::class.java)
    }

    private lateinit var map: Map
    private val REQUEST_CODE_ASK_PERMISSIONS = 1
    private val RUNTIME_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.CHANGE_NETWORK_STATE,
        Manifest.permission.CHANGE_WIFI_STATE
    )

    private val positioningListener = object: PositioningManager.OnPositionChangedListener {
        override fun onPositionFixChanged(
            p0: PositioningManager.LocationMethod?,
            p1: PositioningManager.LocationStatus?
        ) {
            Log.d(TAG, "onPositionFixChanged($p0, $p1)")
        }

        override fun onPositionUpdated(
            p0: PositioningManager.LocationMethod?,
            geoPosition: GeoPosition?,
            p2: Boolean
        ) {
            Log.d(TAG, "locationMethod = $p0, isMapMatched = $p2")
            geoPosition?.let {
                show("${it.coordinate.latitude}, ${it.coordinate.longitude}")
                Log.d(TAG, "source = ${it.positionSource} (${it.positionSource.toPositionSource()}), tech = ${it.positionTechnology} (${it.positionTechnology.toPositionTech()}), accuracy = (${it.latitudeAccuracy}, ${it.longitudeAccuracy})")
                example_map_view.positionIndicator.isVisible = true
                example_map_view.map.setCenter(it.coordinate, Map.Animation.BOW)
                PositioningManager.getInstance().stop()
            }
        }

    }

    private fun Int.toPositionSource() = when(this) {
        0 -> "SOURCE_NONE"
        1 -> "SOURCE_ONLINE"
        2 -> "SOURCE_OFFLINE"
        4 -> "SOURCE_CACHE"
        8 -> "SOURCE_INDOOR"
        16 -> "SOURCE_HARDWARE"
        32 -> "SOURCE_FUSION"
        else -> "SOURCE_UNKNOWN"

    }

    private fun Int.toPositionTech() = when(this) {
        0 -> "TECHNOLOGY_NONE"
        1 -> "TECHNOLOGY_WIFI"
        2 -> "TECHNOLOGY_CELL"
        4 -> "TECHNOLOGY_BLE"
        8 -> "TECHNOLOGY_GNSS"
        16 -> "TECHNOLOGY_SENSORS"
        else -> "SOURCE_UNKNOWN"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_main)
        btn_location.isEnabled = false

        Log.d(TAG, "HERE SDK version: ${Version.getSdkVersion()}")
        btn_location.setOnClickListener { locateMe() }

        if (hasPermissions(this, *RUNTIME_PERMISSIONS)) {
            initHereSdk()
        } else {
            ActivityCompat
                .requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS)
        }
    }

    private fun hasPermissions(context: Context, vararg permissions: String): Boolean {
        permissions.forEach {
            if (ActivityCompat.checkSelfPermission(context, it)
                != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_ASK_PERMISSIONS -> {
                permissions.forEachIndexed {index, permission ->
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {

                        if (!ActivityCompat
                                .shouldShowRequestPermissionRationale(this, permissions[index])) {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                    + " not granted. "
                                    + "Please go to settings and turn on for sample app",
                                Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                    + " not granted", Toast.LENGTH_LONG).show()
                        }
                    }
                }

                initHereSdk()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun initHereSdk() {

        val diskCacheRoot =
            filesDir.path + File.separator + ".isolated-here-maps"
        var intentName: String? = ""
        try {
            val ai = packageManager
                .getApplicationInfo(packageName, PackageManager.GET_META_DATA)
            val bundle = ai.metaData
            val intentKey = getString(R.string.here_sdk_service_intent_name)
            Log.d(TAG, "intentKey = $intentKey")
            intentName = bundle.getString(intentKey)
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e(TAG, "Failed to find intent name, NameNotFound: ", e)
        }

        Log.d(TAG, "diskCacheRoot = $diskCacheRoot, intentName = $intentName")
        val success = MapSettings.setIsolatedDiskCacheRootPath(diskCacheRoot, intentName)
        if(!success) {
            Log.d(TAG, "setIsolatedDiskCacheRootPath was unsuccessful")
            return
        }



        MapEngine.getInstance().init(ApplicationContext(this)) {
            if (it == OnEngineInitListener.Error.NONE) {
                Log.d(TAG, "MapEngine init complete")
                map = Map()
                example_map_view.map = map
                btn_location.isEnabled = true
            } else {
                show("MapEngine init failed; error = $it, details = ${it.details}")
            }
        }
    }

    private fun locateMe() {
        Log.d(TAG,"LocationManager islocationEnabled = ${LocationManagerCompat.isLocationEnabled(locationManager)}")
        locationManager.allProviders.forEach {
            Log.d(TAG, "$it: isEnabled = ${locationManager.isProviderEnabled(it)}")
        }

        val locationMode = when(Settings.Secure.getInt(contentResolver, Settings.Secure.LOCATION_MODE)) {
            LOCATION_MODE_HIGH_ACCURACY -> "LOCATION_MODE_HIGH_ACCURACY"
            LOCATION_MODE_SENSORS_ONLY -> "LOCATION_MODE_SENSORS_ONLY"
            LOCATION_MODE_BATTERY_SAVING -> "LOCATION_MODE_BATTERY_SAVING"
            LOCATION_MODE_OFF -> "LOCATION_MODE_OFF"
            else -> "Unknown"
        }

        Log.d(TAG, "Location mode = $locationMode")
        val hereDataSource = LocationDataSourceHERE.getInstance(object: StatusListenerAdapter(){
            override fun onServiceError(p0: StatusListener.ServiceError?) {
                super.onServiceError(p0)
                Log.d(TAG, "onServiceError: $p0")
            }

            override fun onGnssLocationDisabled() {
                super.onGnssLocationDisabled()
                Log.d(TAG, "onGnssLocationDisabled")
            }

            override fun onAirplaneModeEnabled() {
                super.onAirplaneModeEnabled()
                Log.d(TAG, "onAirplaneModeEnabled")
            }

            override fun onBluetoothDisabled() {
                super.onBluetoothDisabled()
                Log.d(TAG, "onBluetoothDisabled")
            }

            override fun onNetworkLocationDisabled() {
                super.onNetworkLocationDisabled()
                Log.d(TAG, "onNetworkLocationDisabled")
            }

            override fun onCellDisabled() {
                super.onCellDisabled()
                Log.d(TAG, "onCellDisabled")
            }

            override fun onOfflineModeChanged(p0: Boolean) {
                super.onOfflineModeChanged(p0)
                Log.d(TAG, "onOfflineModeChanged: $p0")
            }

            override fun onWifiScansDisabled() {
                super.onWifiScansDisabled()
                Log.d(TAG, "onWifiScansDisabled")
            }

            override fun onPositioningError(p0: StatusListener.PositioningError?) {
                super.onPositioningError(p0)
                Log.d(TAG, "onPositioningError: $p0")
            }
        })
        Log.d(TAG, "hereDataSource = $hereDataSource")
        hereDataSource.setDiagnosticsListener {
            Log.d(TAG, "Diagnostic event: ${it.category} ${it.description}")
        }
        val pm = PositioningManager.getInstance()
        if (hereDataSource != null && USE_NETWORK_POSITIONING) {
            val setDataSourceResult = pm.setDataSource(hereDataSource)
            Log.d(TAG, "setDataSourceResult = $setDataSourceResult; isActive = ${pm.isActive}")
        }
        pm.removeListener(positioningListener)
        pm.addListener(WeakReference<PositioningManager.OnPositionChangedListener>(positioningListener))
        if (pm.start(PositioningManager.LocationMethod.GPS_NETWORK)) {
            Log.d(TAG, "PositioningManager started successfully")
        } else {
            Log.d(TAG, "PositioningManager failed to start")
        }
    }

    override fun onResume() {
        super.onResume()
        example_map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        example_map_view.onPause()
    }

    private fun show(text: String) {
        Log.d(TAG, text)
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }
}